package streamExample;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Sample {

	public static List <Person> createPeople(){
		return Arrays.asList(
				new Person("Sara", Gender.FEMALE, 20),
	 			new Person("Sara", Gender.FEMALE, 22),
				new Person("Bob", Gender.MALE, 20),
				new Person("Paula", Gender.FEMALE, 32),
				new Person("Paul", Gender.FEMALE, 32),
				new Person("Jack", Gender.MALE, 2),
				new Person("Jack", Gender.MALE, 72),
				new Person("Jill", Gender.FEMALE, 12) 
			);
	}
	
	public static boolean isPrime(int number) {
		
		return number > 1 && 
				IntStream.range(2, number)
					.noneMatch(i -> number % i == 0);
	}
	
	
	public static void main(String[] args) {
 
		List<Person> people = createPeople();
 
		printPeople(people);
		
		createPeopleImperitive(people);
		createPeopleFunctional(people);
  
		System.out.println(computeImperitive(51,101));
		System.out.println(computeFunctional(51,101));
		
	}

	//lambda
	public static void printPeople(List<Person> people) {
		
		for(Person person : people) {
 			System.out.println(person.getName());
		}
		
		//for person in people..
		people.forEach(person -> System.out.println(person.getName()));
	}
	
	//streams
	public static void createPeopleImperitive(List<Person> people) {

		for(Person person : people) {
			if(person.getGender() == Gender.FEMALE)
				System.out.println(person.getName().toUpperCase());
		}		
	}
	
	public static void createPeopleFunctional(List<Person> people) {

		//List<String> peeps = people.stream()
		
		people.stream()
				.filter(person -> person.getGender() == Gender.FEMALE)
				.map(Person::getName)
				.map(String::toUpperCase)
				.forEach(System.out::println);
				
		//.collect(Collectors.toList());
  	}

	
	public static double computeImperitive(int n, int k) {
		int index = n;
		int count = 0;
		double total = 0;
		
		while(count < k) {
			if(isPrime(index)) {
				total += Math.sqrt(index);
				count++;
			}
			index++;
		}
		return total;
	}
	 
//find the total of sqrt of first k prime numbers starting from n

	public static double computeFunctional(int n, int k) {
		
		return Stream.iterate(n, e -> e + 1)			// infinite stream
				.filter(Sample::isPrime)				// infinite stream of prime numbers
				.limit(k)                               // not 'less than k', == k..   just 'k'
				.map(Math::sqrt)						// apply square root function to each element
				.reduce(0.0, Double::sum);				// generates result as single element
	}	
 	
}
