package streamExample;

class Person { 
	   Gender gender; 
	   String name; 
	   int age;
	   
	   Person(String name, Gender gender, int age){
		this.name = name;
		this.gender = gender;
		this.age = age;
	   }
	   
	   public Gender getGender() { return gender; }
	   public String getName() { return name; }
	   public int getAge() {return age;}
}
	enum Gender { MALE, FEMALE, OTHER }
